import { BienesnacionalesPage } from './app.po';

describe('bienesnacionales App', function() {
  let page: BienesnacionalesPage;

  beforeEach(() => {
    page = new BienesnacionalesPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});

import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  //templateUrl: './app.component.html',
  template: '<h1>{{title}}</h1><estudiantes [mi-universidad]="laUniversidad"(seleccionado)="mostrarEstudiante($event)"></estudiantes>',
  styleUrls: ['./app.component.css'],
})

export class AppComponent {
  title = 'Bienvenidos a mi primer app, con angular2';
  laUniversidad = "universidad nacional";

  mostrarEstudiante(evento):void{
  	console.log(evento.nombre);
  }
}

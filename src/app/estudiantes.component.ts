import { Component,Input, Output, EventEmitter } from '@angular/core'
import { EstudiantesService } from './estudiantes.service'

@Component({
	selector: 'estudiantes',
	templateUrl: './templates/estudiantes.template.html',
    providers: [EstudiantesService],

})
export class EstudiantesComponent{
	@Input('mi-universidad') universidad: string;
	@Output() seleccionado = new EventEmitter();

	titulo = "Lista de estudiantes";
	titulo_pagina = "Opciones:";
	estudiantes:string[];
	//imgUrl = "";
	redondeadas =  false
	padding = true;

	constructor(estudiantesService:EstudiantesService){
		this.estudiantes = estudiantesService.listaDeEstudiantes('universidad nacional');
	} 

	cambiar(evento){
		this.titulo =  evento.target.value;
	}
	//--
	/*listaDeEstudiantes():Array<string>{
		if(this.universidad == 'universidad nacional'){
			return ['Gabriela Carranza','Irwin Eguiluz','Juan perez'];
		}else{
			return ['Gianni Santucci', 'Nelly Moreno', 'Luis Ramiéz'];
		}
	}*/
	//--
	clickEnEstudiante(evento):void{
		//EMitiendo evento del tipo seleccionado
		this.seleccionado.emit({nombre: evento.target.textContent});
	}

}